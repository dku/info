# Ground Truth: Plattform-Vergleich in Vorlesung mit 70 Teilnehmern

Kontakt: Dirk.Kutscher@hs-emden-leer.de

Hier einige Mess- und Umfrageergebnisse für einen Test mit 5 unterschiedlichen Plattformen, den ich am 17.3.2020 um 17:30 in meiner Vorlesung "Rechnernetze" vorgenommen habe.

In der Vorlesung habe ich nacheinander 5 verschiedene Plattformen getestet und die Studierenden gebeten, die Qualität nach folgenden Kriterien zu bewerten:

* Kann man der Session überhaupt beitreten (Ja/Nein)
* Audio-Qualität (1-6)
* Video-Qualität (1-6)
* Präsentations-Qualität (1-6)

Zusätzlich habe ich die Studierenden noch gebeten, vor der Vorlesung die Qualität ihrer Internet-Anbindung zu messen (Roundtrip-Zeit, Downstream- und Upstream-Durchsatz). Während der gesamten Vorlesung gab es einen Feedback-Kanal (Moodle-Chat), über den wir uns synchronisiert haben und Probleme geschildert haben.


Zusammenfassend lässt sich folgendes sagen:

* Die Internet-Anbindung der Studierenden ist im Durchschnitt sehr gut. Der geringste gemessene Downstream-Durchsatz war 5 MBit/s -- immer noch mehr als ausreichend.
* Am besten wurden WebEx und YouTube-Live-Streaming bewertet
* Beide Varianten von DFNConf -- obwohl hier noch nicht einmal sehr negativ bewertet -- haben (auch um 17:30) nicht sehr gut funktioniert. Laut Feedback während der Vorlesung ist es einigen nicht gelungen, der Session beizutreten. Die Streaming-Variante hat für einige Teilnehmer überhaupt nicht funktioniert.

Hier die Übersicht der Qualitätsbeurteilung im Diagramm:

![alt text](media/image001.png "Erfolgreich Beitreten")

![alt text](media/image002.png "Qualitaetsvergleich")

Und als Tabelle:

|                                      |  |  | Erfolgreich beigetreten | Average Audio-Qualität | Average Video-Qualität | Average Präsentations-Qualität | Average Gesamt-Qualität |
|--------------------------------------|--|--|-------------------------|------------------------|------------------------|--------------------------------|-------------------------|
| **DFNConf (Pexip, conference mode)**     |  |  | 65,45%                  | 30,70%                 | 64,91%                 | 71,49%                         | 55,70%                  |
| **DFNConf (Pexip, streaming mode)**      |  |  | 32,73%                  | 31,14%                 | 33,33%                 | 35,09%                         | 33,19%                  |
| **Jitsi (meet.technik-emden.de)** |  |  | 61,11%                  | 43,42%                 | 46,05%                 | 57,46%                         | 48,98%                  |
| **WebEx**                                |  |  | 61,11%                  | 77,63%                 | 76,75%                 | 80,26%                         | 78,22%                  |
| **YouTube-Live**                         |  |  | 68,52%                  | 74,56%                 | 74,12%                 | 70,18%                         | 72,95%                  |

Weitere Anmerkungen:

* Jitsi über meet.technik-emden.de hat im Prinzip auch gut funktioniert, allerdings gab es einige Probleme mit der Client-Software in einige Browsern. Es gibt eine standalone Variante als Desktop-Anwendung, die besser läuft (wird zur Zeit getestet).
* YouTube-Live Streaming funktioniert gut, allerdings gibt es zwei Dinge zu beachten:
  1. Eignet sich für interaktive Formate (einziger Feedback-Kanal ist die Live-Kommentar-Funktion).
  2. Es gibt eine Verzögerung im Bereich von 1 bis 5 Sekunden. Wir haben das im Versuch getestet, allerdings lassen sich wegen der nicht unerheblichen zusätzlichen Verzögerung durch Moodle-Chat keine absoluten Zahlen nennen.
  3. YouTube-Live erfordert die Verwendung spezieller Broadcast-Software, die etwas technisches Verständnis bzw. Erfahrung erfordert.

Im folgenden die detaillierten Mess- und Umfrageergebnisse:

## Anbindung

| **Anbindung**    |          |                                |                              |  |
|--------------|----------|--------------------------------|------------------------------|--|
|              |          |                                |                              |  |
|              | RTT (ms) | Downstream-Throughput (Mbit/s) | Upstream-Throughput (Mbit/s) |  |
| Mean         | 26       | 50,50                          | 13,00                        |  |
| Average      | 26       | 94,74                          | 27,76                        |  |
| Min          | 2        | 5,00                           | 1,00                         |  |
| Max          | 60       | 989,00                         | 160,00                       |  |
| keine Angabe | 2        | 2,00                           | 5,00                         |  |




## DFNConf (Pexip, conference mode)

| **DFNConf (Pexip, conference mode)** |         |         |
|----------------------------------|---------|---------|
|                                  | absolut | %       |
| Erfolgreich beigetreten          | 36      | 65,45%  |
| Nicht erfolgreich beigetreten    | 2       | 3,64%   |
| Keine Angabe                     | 17      | 30,91%  |
|                                  |         |         |
| **Audio-Qualität (1 - 6)**           |         |         |
| Mean                             | 1,00    | 16,67%  |
| Average                          | 1,84    | 30,70%  |
| Min                              | 1,00    | 16,67%  |
| Max                              | 6,00    | 100,00% |
| keine Angabe                     | 17,00   |         |
|                                  |         |         |
| **Video-Qualität (1 - 6)**           |         |         |
| Mean                             | 5,00    | 83,33%  |
| Average                          | 3,89    | 64,91%  |
| Min                              | 1,00    | 16,67%  |
| Max                              | 6,00    | 100,00% |
| keine Angabe                     | 17,00   |         |
|                                  |         |         |
| **Präsentations-Qualität (1 - 6)**   |         |         |
| Mean                             | 5,00    | 83,33%  |
| Average                          | 4,29    | 71,49%  |
| Min                              | 1,00    | 16,67%  |
| Max                              | 6,00    | 100,00% |
| keine Angabe                     | 17,00   |         |


## DFNConf (Pexip, streaming mode)

| **DFNConf (Pexip, streaming mode)** |         |         |
|---------------------------------|---------|---------|
|                                 | absolut | %       |
| Erfolgreich beigetreten         | 18      | 32,73%  |
| Nicht erfolgreich beigetreten   | 20      | 36,36%  |
| Keine Angabe                    | 17      | 30,91%  |
|                                 |         |         |
| **Audio-Qualität (1 - 6)**          |         |         |
| Mean                            | 1,00    | 16,67%  |
| Average                         | 1,87    | 31,14%  |
| Min                             | 1,00    | 16,67%  |
| Max                             | 6,00    | 100,00% |
| keine Angabe                    | 17,00   |         |
|                                 |         |         |
| **Video-Qualität (1 - 6)**          |         |         |
| Mean                            | 1,00    | 16,67%  |
| Average                         | 2,00    | 33,33%  |
| Min                             | 1,00    | 16,67%  |
| Max                             | 6,00    | 100,00% |
| keine Angabe                    | 17,00   |         |
|                                 |         |         |
| **Präsentations-Qualität (1 - 6)**  |         |         |
| Mean                            | 1,00    | 16,67%  |
| Average                         | 2,11    | 35,09%  |
| Min                             | 1,00    | 16,67%  |
| Max                             | 6,00    | 100,00% |
| keine Angabe                    | 17,00   |         |

## Jitsi (meet.technik-emden.de)

| **Jitsi (meet.technik-emden.de)**        |         |         |
|--------------------------------------|---------|---------|
|                                      | absolut | %       |
| Erfolgreich beigetreten              | 33      | 61,11%  |
| Nicht erfolgreich beigetreten        | 5       | 9,26%   |
| Keine Angabe                         | 16      | 29,63%  |
|                                      |         |         |
| **Audio-Qualität (1 - 6)**               |         |         |
| Mean                                 | 2,00    | 33,33%  |
| Average                              | 2,61    | 43,42%  |
| Min                                  | 1,00    | 16,67%  |
| Max                                  | 6,00    | 100,00% |
| keine Angabe                         | 17,00   |         |
|                                      |         |         |
| **Video-Qualität (1 - 6)**               |         |         |
| Mean                                 | 3,00    | 50,00%  |
| Average                              | 2,76    | 46,05%  |
| Min                                  | 1,00    | 16,67%  |
| Max                                  | 6,00    | 100,00% |
| keine Angabe                         | 17,00   |         |
|                                      |         |         |
| **Präsentations-Qualität (1 - 6)**       |         |         |
| Mean                                 | 3,00    | 50,00%  |
| Average                              | 3,45    | 57,46%  |
| Min                                  | 1,00    | 16,67%  |
| Max                                  | 6,00    | 100,00% |
| keine Angabe                         | 17,00   |         |

## WebEx

| **WebEx**                          |         |         |
|--------------------------------|---------|---------|
|                                | absolut | %       |
| Erfolgreich beigetreten        | 33      | 61,11%  |
| Nicht erfolgreich beigetreten  | 5       | 9,26%   |
| Keine Angabe                   | 16      | 29,63%  |
|                                |         |         |
| **Audio-Qualität (1 - 6)**         |         |         |
| Mean                           | 5,00    | 83,33%  |
| Average                        | 4,66    | 77,63%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |
|                                |         |         |
| **Video-Qualität (1 - 6)**         |         |         |
| Mean                           | 5,00    | 83,33%  |
| Average                        | 4,61    | 76,75%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |
|                                |         |         |
| **Präsentations-Qualität (1 - 6)** |         |         |
| Mean                           | 5,00    | 83,33%  |
| Average                        | 4,82    | 80,26%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |

## YouTube-Live

| **YouTube-Live**                   |         |         |
|--------------------------------|---------|---------|
|                                | absolut | %       |
| Erfolgreich beigetreten        | 37      | 68,52%  |
| Nicht erfolgreich beigetreten  | 1       | 1,85%   |
| Keine Angabe                   | 16      | 29,63%  |
|                                |         |         |
| **Audio-Qualität (1 - 6)**         |         |         |
| Mean                           | 5,00    | 83,33%  |
| Average                        | 4,47    | 74,56%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |
|                                |         |         |
| **Video-Qualität (1 - 6)**         |         |         |
| Mean                           | 5,00    | 83,33%  |
| Average                        | 4,45    | 74,12%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |
|                                |         |         |
| **Präsentations-Qualität (1 - 6)** |         |         |
| Mean                           | 4,00    | 66,67%  |
| Average                        | 4,21    | 70,18%  |
| Min                            | 1,00    | 16,67%  |
| Max                            | 6,00    | 100,00% |
| keine Angabe                   | 17,00   |         |



