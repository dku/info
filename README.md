# Online-Tools für Lehrveranstaltungen

Hier einige Empfehlungen für Online-Tools in der aktuellen Covid-19-Krise.

Generell ist DNFConf die Plattform der Wahl, weil es den richtigen Funktionsumfang bietet und ggf. dem einen oder anderen auch schon bekannt ist. Es ist allerdings nicht ausszuschließen, daß es zur Kapazitätsproblemen führt, wenn alle Hochschulen Deutschland am 16.3. anfangen, ihre Vorlesungen online zu machen. Als Fallback ist daher durchaus sinnvoll, sich mit Alternativen zu beschäftigen, wie z.B. WebEx oder Jitsi (auch wenn diese nicht direkt für die Lehre sondern eher für Conference-Calls gedacht sind).

Bei Fragen oder Anregungen: dirk.kutscher@hs-emden-leer.de

* Update 2020-03-19: [Plattform-Vergleich](plattform-vergleich.md) 



## DFNConf

Die Lösung, die normalerweise im Online-Studium von uns verwendet wird.

* https://www.conf.dfn.de
* Verwendet AdobeConnect
* Lizenzen für viele Hochschulen inklusive HSEL
* Wird laut DFN möglicherweise Kapazitätsprobleme bekommen

### Hinweise von Tjarko Tjaden

* Einloggen: https://my.conf.dfn.de/pexip/
* Neue Vorlesung klicken
* Gäste-PIN bei Bedarf setzen
* Gäste dürfen präsentieren: nein
* Streaming aktivieren: ja
* Streaming Chat: ja
* Layout: Full-Screen Main Speaker only
* Alle Gäste stummschalten: ja
* Präsentierer als Hauptsprecher festlegen: ja

* Die angezeigte Teilnehmer-Begrenzung gilt nur für das direkte teilnehmen in der Konferenz. Über den Streaming-Link + Chat-Fenster können beliebig viele Personen teilnehmen.
* [Anleitungs-Video](https://re-lab.hs-emden-leer.de/s/WMtj4HcTy6aHX9Q) 
* Hinweise: Den erzeugten "virtuellen Vorlesungsraum" können Sie nun beliebig häufig verwenden.

### Tips

* Unterlagen (Folien) vorher über Moodle etc. verfügbar machen
* Video: Auflösung reduzieren oder ggf. ganz einstellen

## Cisco WebEx

Professionelles Conferencing-Tool in der Cloud

* Cisco verteilt zur Zeit kostenlose WebEx-Lizenzen
* bis zu 200 Teilnehmer, mit Application Sharing, Aufzeichnung, Chat, Telefoneinwahl
* siehe [Anleitung](https://rn-git.technik-emden.de/dku/info/-/raw/master/media/webex_Angebot_corona_generic_20200313.pptx)
* Direkter Link zur Anmeldung: https://cart.webex.com/sign-up?utm_source=hpbanner&utm_medium=website&utm_campaign=COVID19
* Client-Anwendungen oder Browser-basiert

## Rechnernetze-Jitsi-Conferencing-Server

Lokaler Conferencing-Server

* https://meet.technik-emden.de
* Video-Streaming, Application-Sharing
* In Vorbereitung: shared editing über Etherpad, Telefoneinwahl
* Läuft im Browser (Chrome, Chromium, Firefox)
* Account wird nicht benötigt: einfach verwenden
* Für Vorlesungen:
  * drei vertikale Punkten unten rechts -> Settings ->More
  * dann “Everyone starts muted” / “Everyone follows me”

## Streaming über YouTube-Live

Aus technischer Sicht vermutlich die skalierbarste Lösung, da YouTube (Google) einfach über ausreichend Resourcen und insbesondere über zahlreiche Edge-Caches (so z.B. bei EweTel) verfügt

* https://www.youtube.com/live_dashboard?nv=1
* Achtung: man muss sich 24-Stunden vor dem ersten Streaming anmelden (aus juristischen Gründen)
* Man benötigt dazu eine Broadcasting-Software wie z.B. [OBS](https://obsproject.com)
* Die Studierenden können natürlich ganz normal ihren Browser verwenden
* Bei Kapazitätsproblemen die Video-Qualität anpassen (Default-mässig HD)

## Weitere Tools

Weitere Tools zur lokalen Installation:

* [BigBlueButton](https://bigbluebutton.org/): Web Conferencing for Online Learning
* [EJabberD](https://www.ejabberd.im): Instant Messaging
* [Etherpad](https://etherpad.org/): Collaborative Text Editing

## Methodik

* https://brightspace-support.tudelft.nl/remote-teaching-learning/






